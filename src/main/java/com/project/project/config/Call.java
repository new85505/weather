package com.project.project.config;

import com.project.project.model.Feedback;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class Call implements CommandLineRunner {

    private static void callRestService() {
        RestTemplate restTemplate = new RestTemplate();
        Feedback feedback = restTemplate.getForObject("http://localhost:8080/feedback/7",Feedback.class);
        System.out.println(feedback.getName());
    }

    @Override
    public void run(String... args) throws Exception {
        callRestService();
    }
}
