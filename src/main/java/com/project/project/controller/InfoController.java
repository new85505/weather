package com.project.project.controller;

import com.project.project.model.Info;
import com.project.project.service.InfoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

@RestController
public class InfoController {

    private final InfoService infoService;

    @Autowired
    private final RestTemplate restTemplate;

    @Autowired
    public InfoController(InfoService infoService, RestTemplate restTemplate) {
        this.infoService = infoService;
        this.restTemplate = restTemplate;
    }

    Logger logger = LoggerFactory.getLogger(InfoController.class);

    @GetMapping(value = "/weather/{name}")
    public List<Info> findByName(@PathVariable("name") String name) {
        Info info = infoService.findWeatherByCityName(name);
        ResponseEntity responseEntityOk = new ResponseEntity(info, HttpStatus.OK);
        String url = "https://samples.openweathermap.org/data/2.5/weather?q=London,uk&appid=b6907d289e10d714a6e88b30761fae22";
        Info[] infos = restTemplate.getForObject(url, Info[].class, name);
        logger.info("Info found " + responseEntityOk);
        return Arrays.asList(infos);
    }
}
