package com.project.project.repository;

import com.project.project.model.Feedback;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CallRepository extends JpaRepository<Feedback, Long> {
}
