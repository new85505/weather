package com.project.project.service;

import com.project.project.model.Info;

public interface InfoService {
    public Info findWeatherByCityName(String name);
    public Iterable<Info> list();
}
