package com.project.project.service.impl;

import com.project.project.model.Info;
import com.project.project.repository.InfoRepository;
import com.project.project.service.InfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class InfoServiceImpl implements InfoService {

    private final InfoRepository infoRepository;

    @Autowired
    public InfoServiceImpl(InfoRepository infoRepository) {
        this.infoRepository = infoRepository;
    }

    @Override
    public Info findWeatherByCityName(String name) {
        return infoRepository.findByName(name);
    }

    @Override
    public Iterable<Info> list() {
        return infoRepository.findAll();
    }
}
